package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	

	public int sumAllIntegers(IntegersBag bag){
		int sum = 0;
	   
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				sum = sum + iter.next();
				
			}
			
		}
		return sum;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int temp = 0;
	   
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				temp = iter.next();
				if (min > temp) 
				{
					min = temp;
				}
				
				
			}
			
		}
		return min;
	}
	
	public String toBinary(IntegersBag bag){
		String binaryInteger = "";
		String allNumbers = "";
		int number;
	   
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				number = iter.next();
				binaryInteger = Integer.toBinaryString(number);
				allNumbers = allNumbers + " " + binaryInteger;	
			}
			
		}
		return allNumbers;
	}
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
}
